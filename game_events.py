import utils
import settings
from xml.dom.minidom import *

def LoadEventsListFromXML(platform, version):
    events_list = []
    xml = parse('coem_settings\\{platform}\\{version}\\analytics_events.xml'.format(platform = platform, version = version))
    node = xml.getElementsByTagName('AwemAnalytics').item(0)
    for event in node.childNodes:
        if event.nodeName == 'Event':
            events_list.append(event.getAttribute('title'))
    #for key in events_list:
        #utils.log(key)
    return events_list

def LoadEventsListWithParamsFromXML(platform, version):

    events_list = dict()
    xml = parse('coem_settings\\{platform}\\{version}\\analytics_events.xml'.format(platform = platform, version = version))
    node = xml.getElementsByTagName('AwemAnalytics').item(0)

    for events in node.childNodes:
        if events.nodeName == 'Event':
            allEvents = dict()

            titleEvent = events.getAttribute('title')

            paramsCommon = events.getElementsByTagName('Common').item(0)
            parseEvent(paramsCommon, allEvents, 'Common')

            paramsSpecific = events.getElementsByTagName('Specific').item(0)
            parseEvent(paramsSpecific, allEvents, 'Specific')

            events_list[titleEvent] = allEvents

        def parseEvent(paramsList, Events, TypeEvents):
            if paramsList is not None:
                for param in paramsList.childNodes:
                    if param.nodeName == 'Param':
                        title = param.getAttribute('title')
                        required = param.getAttribute('required')
                        disabled = param.getAttribute('disabled')
                        event = {"Type": TypeEvents, "Required": required, "Disabled": disabled}
                        Events[title] = event

            return

    return events_list

def LoadEventsListFromSQL(platform, version):
    events_list = []
    r, m = utils.ExecuteMySQL("select distinct action from b_viral_stat where version = '{version}'".format(version = version), platform)
    if r:
        for events_map in m:
            for key in events_map:
                events_list.append(key)
        #for key in events_list:
            #utils.log(key)
    return events_list

def LoadEventsListWithParamsFromSQL(platform, version):
    events_list = []
    r, m = utils.ExecuteMySQL("select action,data from b_viral_stat where version = '{version}' limit 2".format(version = version), platform )
    if r:
        for events_map in m:
            for key in events_map:
                events_list.append(key)

    temp_event_list = []
    keyValue = ''
    dictEvent = dict()

    count = 0
    for value in events_list:
        if count % 2 == 0:
            keyValue = value
        else:
            dictEvent[keyValue] = value
            temp_event_list.append(dictEvent)

            dictEvent = dict()
            params = ''
        count = count + 1

    parse_events_list = []

    for events in temp_event_list:
        dictAction = dict()
        for event, params in events.iteritems():
            strParseParams = params.split("|")

            dictParams = dict()

            for events in strParseParams:
                index = events.index('=')
                lenght = len(events)

                dictParams[events[0:index]] = events[index + 1:lenght]

                dictAction[event] = dictParams

        parse_events_list.append(dictAction)
        
    return parse_events_list

'''def LoadEventsListFromHDP(platform, version):
    events_list = []
    r, m = utils.ExecuteHiveQL("select distinct i_action from {database}{platform}.events where i_version = '{version}'".format(
        database = settings.db_name_rstat,
        platform = platform,
        version = version
    ))
    if r:
        events_list = m.split('\n')
    #for key in events_list:
        #utils.log(key)
    return events_list'''

def CheckEventsList(platform, version):
    utils.log('\n--- Check events for: platform {platform} version {version} ---\n'.format(platform = platform, version = version))
    events_xml = LoadEventsListFromXML(platform, version)
    events_sql = LoadEventsListFromSQL(platform, version)
    #events_hdp = LoadEventsListFromHDP(platform, version)

    # missed events debug version
    utils.log('\nMissed events (debug):\n')
    for event_name in events_xml:
        if event_name not in events_sql:
            utils.log(' ' + event_name)

    # unnormal events debug version
    utils.log('\nUnnormal events (debug):\n')
    for event_name in events_sql:
        if event_name not in events_xml:
            utils.log(' ' + event_name)

    # missed events production
    utils.log('\nMissed events (production):\n')
    for event_name in events_xml:
        '''if event_name not in events_hdp:
            utils.log(' ' + event_name)'''

    # unnormal events production
    utils.log('\nUnnormal events (production):\n')
    '''for event_name in events_hdp:
        if event_name not in events_xml:
            utils.log(' ' + event_name)'''

def LoadPacksFromMySQL(platform, version):
    packs_list = []
    r, m = utils.ExecuteMySQL("""
        select distinct
            substr(substr(data, locate('FullAction=', data) + length('FullAction=')), 1, locate('|', substr(data, locate('FullAction=', data) + length('FullAction='))) - 1)
        from b_viral_stat
        where version = '{version}' and action = 'Buy' and data like '%FullAction=credits-com.awem%'
    """.format(version = version), platform)
    if r:
        for events_map in m:
            for key in events_map:
                packs_list.append(key)
        #for key in packs_list:
            #utils.log(key)
    return packs_list

def LoadPacksFromHDP(platform, version):
    packs_list = []
    r, m = utils.ExecuteHiveQL("""
        select distinct
            i_data['FullAction']
        from {database}{platform}.events
        where i_version = '{version}' and i_action = 'Buy' and i_data['FullAction'] like '%credits-com.awem%'
        """.format(
        database = settings.db_name_rstat,
        platform = platform,
        version = version
    ))
    if r:
        packs_list = m.split('\n')
    #for key in packs_list:
        #utils.log(key)
    return packs_list

'''def LoadPacksFromHDPDictionary(platform, version):
    packs_list = []
    r, m = utils.ExecuteHiveQL("""
        select distinct
            pack_id
        from {database}{platform}.packs
        """.format(
        database = settings.db_name_rstat,
        platform = platform,
        version = version
    ))
    if r:
        packs_list = m.split('\n')
    #for key in packs_list:
        #utils.log(key)
    return packs_list'''

def CheckPacksList(platform, version):
    utils.log('\n--- Check credits packs for: platform {platform} version {version} ---\n'.format(platform = platform, version = version))
    packs_mysql = LoadPacksFromMySQL(platform, version)
    packs_hdp = LoadPacksFromHDP(platform, version)
    packs_dict = LoadPacksFromHDPDictionary(platform, version)

    utils.log('\nRegistered in HDP packs:\n')
    for pack_name in packs_dict:
        utils.log(' ' + pack_name)

    utils.log('\nPacks in debug:\n')
    for pack_name in packs_mysql:
        utils.log(' ' + pack_name)

    utils.log('\nPacks in production:\n')
    for pack_name in packs_hdp:
        utils.log(' ' + pack_name)
