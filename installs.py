import utils
import settings

def LoadInstallsFromAppsflyer(platform):
    installs = {}
    r = utils.ExecuteMSSQL("""
        select
            cast(date as varchar) as cohort,
            sum(installs) as af_inst
        from ads_report.dbo.af_geo
        where app_id='{app_id}'
        group by cast(date as varchar)
        order by cast(date as varchar) desc;
    """.format(app_id = settings.appsflyer_app_id[platform]))
    if r:
        for af_installs in r:
            installs[af_installs[0]] = af_installs[1]
        #for key in installs:
            #utils.log('{0}\t{1}'.format(key, installs[key]))
    return installs

def LoadInstallsFromHDP(platform):
    installs = {}
    r, m = utils.ExecuteHiveQL("""
        select
            i_data['Cohort'] as cohort,
            count(distinct i_device) s_inst
        from {database}{platform}.events
        where i_action = 'FirstStartGame' and i_data['Cohort'] <= date_sub(current_date, 2)
        group by i_data['Cohort']
        order by cohort desc;
        """.format(
        database = settings.db_name_rstat,
        platform = platform
    ))
    if r:
        rows = m.split('\n')
    for key in rows:
        if len(key.split('\t')) >= 2:
            installs[key.split('\t')[0]] = key.split('\t')[1]
    #for key in installs:
        #utils.log('{0}\t{1}'.format(key, installs[key]))
    return installs

def LoadInstallsFromFacebook(platform):
    installs = {}
    r = utils.ExecuteMSSQL("""
        select top {day_num}
            RepDate as cohort,
            sum(Installs) as fb_inst
        from ads_report.dbo.fb_coem_stat
        where platform in ({fb_platform})
        group by RepDate --, platform
        order by RepDate desc;
    """.format(fb_platform = settings.fb_platform_name[platform], day_num = settings.appsflyer_days_to_analize))
    if r:
        for af_installs in r:
            installs[af_installs[0]] = af_installs[1]
        #for key in installs:
            #utils.log('{0}\t{1}'.format(key, installs[key]))
    return installs

def CheckInstalls(platform):
    af_installs = LoadInstallsFromAppsflyer(platform)
    hdp_installs = LoadInstallsFromHDP(platform)
    fb_installs = LoadInstallsFromFacebook(platform)

    keys = list(hdp_installs.keys())
    keys.sort(reverse = True)

    utils.log('cohort\thdp_installs\taf_installs\tfb_inst\tdiff, %\tfb_diff, %\n')
    for cohort in keys[0:settings.appsflyer_days_to_analize - 1]:
        failed = ''
        if cohort not in list(af_installs.keys()):
            af_installs[cohort] = 'None'
            diff = ''
        else:
            diff = round(100 * (int(hdp_installs[cohort]) - int(af_installs[cohort])) / int(hdp_installs[cohort]), 2)
            if abs(diff) > settings.appsflyer_failed_percent_installs:
                failed = 'FAILED'

        if cohort not in list(fb_installs.keys()):
            fb_installs[cohort] = 'None'
            fb_diff = ''
        else:
            fb_diff = round(100 * (int(hdp_installs[cohort]) - int(fb_installs[cohort])) / int(hdp_installs[cohort]), 2)
            if abs(fb_diff) > settings.fb_failed_percent_installs:
                failed = 'FAILED'

        utils.log('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}'.format(
            cohort,
            hdp_installs[cohort],
            af_installs[cohort],
            fb_installs[cohort],
            diff,
            fb_diff,
            failed
        ))
