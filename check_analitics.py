import os
import sys
from datetime import datetime
import utils
import settings
import game_events
import installs
import device_count

if os.path.isfile(settings.pid_file):
    utils.log('{0}: task is already running.'.format(datetime.now()), True)
    exit(1)
open(settings.pid_file, 'w').write(str(os.getpid()))
try:
    start_time = datetime.now()
    utils.CurrentLog('reset')
    utils.log('\n--- start: {0} ---\n'.format(start_time))

    if len(sys.argv) >= 2:

        platform = sys.argv[1]
        if platform not in settings.platform_list:
            utils.log('FAILED: Unknown platform: {0}'.format(platform), True)
            exit(1)

        ### check
        if sys.argv[2] == 'events':
            for ver in settings.check_affected_versions[platform]:
                game_events.CheckEventsList(platform, ver)
                game_events.CheckPacksList(platform, ver)

        elif sys.argv[2] == 'installs':
            installs.CheckInstalls(platform)

        elif sys.argv[2] == 'devices':
            device_count.CheckDeviceCount(platform)

        elif sys.argv[2] == 'test':
            installs.LoadInstallsFromFacebook(platform)

        else:
            utils.log('FAILED: Unknown command: ' + sys.argv[2])

    else:
        utils.log('FAILED: Missing command.', True)
        exit(1)

    end_time = datetime.now()
    utils.log('\n--- end: {0} ---\n'.format(end_time))
    utils.CurrentLog(operation = 'send', subj = platform + ' - ' + sys.argv[2])
    utils.log('Execution time: ' + (end_time - start_time).__str__() + '\n')
finally:
    os.unlink(settings.pid_file)