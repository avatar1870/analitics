#import paramiko
import settings
import sys
import smtplib
from email.mime.text import MIMEText
import pypyodbc as pyodbc # for Python 3.4 use the command "c:\Python34\Scripts\pip.exe install pypyodbc" before
from mysql.connector import MySQLConnection, Error
import utils
import os

def ExecuteMySQL(query, platform):
    result = True
    message = ''
    try:
        conn = MySQLConnection(
            host = settings.mysql_connection_params[platform]['host'],
            database = settings.mysql_connection_params[platform]['database'],
            user = settings.mysql_connection_params[platform]['user'],
            password = settings.mysql_connection_params[platform]['password']
        )
        if conn.is_connected():
            cursor = conn.cursor()
            cursor.execute(query)
            message = cursor.fetchall()
        else:
            utils.log('Connection failed.')
            result = False
    except Error as error:
        utils.log(error)
        result = False
    finally:
        conn.close()
    return result, message

def ExecuteMSSQL(sql_command):
    print(sql_command)
    result = False
    try:
        db_connection = pyodbc.connect(settings.mssql_connection_string, autocommit = True)
        db_cursor = db_connection.cursor()
        try:
            db_cursor.execute(sql_command)
            result = db_cursor.fetchall()
        except:
            utils.log("Error executing query: " + str(sys.exc_info()[0]) + "\n" + str(sys.exc_info()[1]))
        db_cursor.close()
        del db_cursor
        db_connection.close()
    except:
        utils.log("Can't connect to DB: " + str(sys.exc_info()[0]) + "\n" + str(sys.exc_info()[1]))
    return result


'''def ExecuteHiveQL(query):
    try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=settings.host, username=settings.user, password=settings.password, port=settings.port)
        stdin, stdout, stderr = client.exec_command('hive -S -e "' + query + ' "')
        data = stdout.read()
        error = stderr.read()
        result = data.decode('utf-8')
        client.close()
    except Exception as e:
        return False, e
    if ('FAILED' or 'ERROR') in str(error):
        return False, error.decode('utf-8')
    else:
        return True, result'''

def SendLogEmail(subj, message):
    utils.log('Send email...')
    from_mail = settings.mail_from
    to_mail = settings.mail_to
    msg = MIMEText(message)
    msg['Subject'] = subj
    msg['From'] = from_mail
    msg['To'] = to_mail
    s = smtplib.SMTP(settings.mail_server)
    s.sendmail(from_mail, to_mail, msg.as_string())
    s.quit()
    return

def CurrentLog(operation, message = '', subj = ''):
    if operation == 'reset':
        try:
            fv = open(settings.log_current_filename, 'w')
            fv.write('Start operation log...' + '\n')
            fv.close()
        except Exception as e:
            print('Open/write file error. ' + str(e))

    elif operation == 'add':
        try:
            fv = open(settings.log_current_filename, 'a')
            fv.write(message + '\n')
            fv.close()
        except Exception as e:
            print('Open/write file error. ' + str(e))

    elif operation == 'send':
        try:
            fv = open(settings.log_current_filename, 'r')
            log_msg = fv.read()
            fv.close()
            op_status = 'OK'
            if ('FAILED' or 'ERROR') in log_msg:
                op_status = 'FAILED'
            SendLogEmail(subj + ' - ' + op_status, log_msg)
        except Exception as e:
            print('Open/write file error. ' + str(e))
    return

def log(msg, skip_current = False):
    print(msg)
    try:
        fv = open(settings.log_filename, 'a')
        fv.write(msg + '\n')
        fv.close()
        if not skip_current:
            CurrentLog('add', msg)
    except Exception as e:
        print('Open/write file error. ' + str(e))
    return
