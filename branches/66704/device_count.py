import utils
import settings

def LoadDevicesFromAppsflyer(platform):
    '''af_devices = {}
 r, m = utils.ExecuteHiveQL("""
        select
            count(distinct afi.customeruserid) device_count,
            afi.repdate cohort
        from ads.af_installs afi
        where afi.platform = '{platform}'
        and customeruserid not in (
            select device_id from rstat_{platform}.users
        )
        group by afi.repdate
        order by afi.repdate desc
        limit {days}
        ;
    """.format(platform = platform, days = settings.appsflyer_days_to_analize))
    if r:
        rows = m.split('\n')
        for key in rows:
            if len(key.split('\t')) >= 2:
                af_devices[key.split('\t')[1]] = key.split('\t')[0]
        #for key in af_devices:
            #utils.log('{0}\t{1}'.format(key, af_devices[key]))
    return af_devices

def LoadDevicesFromHDP(platform):
    hdp_devices = {}
    r, m = utils.ExecuteHiveQL("""
        select
            count(distinct device_id) device_count,
            Cohort
        from rstat_{platform}.users
        where Cohort in (
            select repdate from ads.af_installs
        )
        group by Cohort
        order by Cohort desc
        limit {days}
        ;
    """.format(platform = platform, days = settings.appsflyer_days_to_analize))
    if r:
        rows = m.split('\n')
        for key in rows:
            if len(key.split('\t')) >= 2:
                hdp_devices[key.split('\t')[1]] = key.split('\t')[0]
        #for key in hdp_devices:
            #utils.log('{0}\t{1}'.format(key, hdp_devices[key]))
    return hdp_devices '''

def CheckDeviceCount(platform):
    '''af_devices = LoadDevicesFromAppsflyer(platform)
    hdp_devices = LoadDevicesFromHDP(platform)

    keys = list(af_devices.keys())
    keys.sort(reverse = True)

    utils.log('cohort\thdp_devices\tmissed_af_devices\tdiff, %\n')
    for cohort in keys:
        failed = ''
        if cohort in list(af_devices.keys()) and cohort in list(hdp_devices.keys()):
            diff = round(100 * (int(af_devices[cohort])) / int(hdp_devices[cohort]), 2)
            if abs(diff) > settings.appsflyer_failed_percent_devices:
                failed = 'FAILED'
            utils.log('{0}\t{1}\t{2}\t{3}\t{4}'.format(
                cohort,
                hdp_devices[cohort],
                af_devices[cohort],
                diff,
                failed
            ))'''