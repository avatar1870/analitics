# coding: utf8
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

# PID params
pid_file = 'check_analitics.pid'

# platforms
platform_list = ['ios', 'android', 'amazon']

# NameNode ssh connection params
host = '192.168.0.41'
user = 'hdpadmin'
password = 'G00dHDP'
port = 22

# MySQL connection params
mysql_connection_params = {
    'ios' : {'host': 'coem.awem.com', 'database': 'coem_social_d', 'user': 'user_clepcha', 'password': 'ClepchaMysqlPass2014'},
    'android' : {'host': 'coem.awem.com', 'database': 'android_coem_social_d', 'user': 'testersreadon', 'password': 'aelZZZocdh90o'},
    'amazon' : {'host': 'coem.awem.com', 'database': 'amazon_coem_d', 'user': 'testersreadon', 'password': 'aelZZZocdh90o'}
}

# HIVE database params
db_name_rstat = 'rstat_'
db_name_ads = 'ads'

# log params
log_filename = 'check_import.log'
log_current_filename = 'check_import_current.log'

# ms-sql params
mssql_connection_string = """
        Driver={SQL Server};
        Server=awem-sql-4;
        Database=ads_report;
        Trusted_Connection=yes;
        """

# check versions params
check_affected_versions = {
    'ios' : ['4.0.0', '4.1.0'],
    'android' : ['3.9.1', '4.0.0'],
    'amazon' : ['3.6.4', '3.6.6']
}

# log e-mail params
mail_server = 'terabyte'
mail_from = 'hdp@awem.local'
mail_to = 'zhuravlev@awem.local'

# appsflyer params
appsflyer_days_to_analize = 100
appsflyer_failed_percent_devices = 10
appsflyer_failed_percent_installs = 10
appsflyer_app_id = {
    'ios' : 'id738480930',
    'android' : 'com.awem.cradleofempires.andr',
    'amazon' : 'com.awem.cradleofempires.amzn-Amazon'
}

# facebook params
fb_platform_name = {
    'ios' : "'ios_unknown', 'ipad', 'iphone'",
    'android' : "'android'",
    'amazon' : ""
}
fb_failed_percent_installs = 50

#e-mail setting
TO='test1galimskiy@gmail.com galimskiy@awem.by igor.galimski5@mail.ru'
SUBJECT='Check'
LOGIN='test2galimskiy'
PASSWORD='test2_galimskiy'
SMTPSERVER='smtp.gmail.com'
SMTPPORT='587'

#issue param
OFF=u'Отключен'
NOTONLIST=u'Нет в списке'
DONTCOME=u'Не пришёл'
EMPTY=u'Пустой'
UNKNOWN=u'Неизвестное событие'