# coding: utf8
from datetime import datetime
import datetime
import game_events
import mail
import settings
import os

def check(platform, version):
    now_time = datetime.datetime.now()
    message = '<font size="3"> <b>' +  u'Проверка начата ' + '<font color="red">' + str(now_time.strftime("%d.%m.%Y %I:%M %p")) + '</font>' + u' для платформы: ' + '<font color="green">' + platform + '</font>' + u' и для версии: ' + '<font color="blue">' + version + '</font> </b> </font>' + '<br /><br />'

    events_list_from_xml = game_events.LoadEventsListWithParamsFromXML(platform, version)

    event_from_xml = game_events.LoadEventsListWithParamsFromSQL(platform, version)
    events_list_from_sql = event_from_xml[0]
    events_list_id_from_sql = event_from_xml[1]

    message += "<font size='2' color = 'Purple'> <b>" + 'Количество событий: ' + str(len(events_list_from_sql)) + "</b> </font> <br /><br />"

    message += "<table cellpadding = '7' border = '2' width = '100%'>"
    message += "<tr bgcolor = '#000080'>"
    message += "<th><font color = 'white' size = '3'>ID</font></th>"
    message += "<th><font color = 'white' size = '3'>Дата</font></th>"
    message += "<th><font color = 'white' size = '3'>Событие</font></th>"
    message += "<th><font color = 'white' size = '3'>Проблема</font></th>"
    message += "<th><font color = 'white' size = '3'>Параметр</font></th>"
    message += "<th><font color = 'white' size = '3'>Значение</font></th>"
    message += "<th><font color = 'white' size = '3'>Тип</font></th>"
    message += "</tr>"

    number = 0
    for events in events_list_from_sql:
        for eventName in events:
            eventInfo = events_list_id_from_sql[number][eventName]
            if eventName in events_list_from_xml:
                for paramName in events[eventName]:
                    eventWithParams = events_list_from_xml[eventName]

                    paramValue = events[eventName][paramName]

                    #Check for empty
                    if (paramValue == ''):
                        message += addRow(eventInfo, eventName, settings.EMPTY, paramName, paramValue, type)

                    #Check input params
                    if paramName in eventWithParams:
                        required = str(events_list_from_xml[eventName][paramName]['Required'])
                        disabled = str(events_list_from_xml[eventName][paramName]['Disabled'])
                        type = str(events_list_from_xml[eventName][paramName]['Type'])

                        required = emptyToZero(required)
                        disabled = emptyToZero(disabled)

                        if(disabled == '1'):
                            message += addRow(eventInfo, eventName, settings.OFF, paramName, paramValue, type)

                    else:
                        message += addRow(eventInfo, eventName, settings.NOTONLIST, paramName, paramValue, '')

                #Check required params
                for paramName in events_list_from_xml[eventName]:

                    required = events_list_from_xml[eventName][paramName]['Required']
                    disabled = events_list_from_xml[eventName][paramName]['Disabled']
                    type = events_list_from_xml[eventName][paramName]['Type']

                    required = emptyToZero(required)
                    disabled = emptyToZero(disabled)

                    if (required == '1' and disabled == '0'):
                        if(paramName not in events[eventName]):
                            message += addRow(eventInfo, eventName, settings.DONTCOME, paramName, '', type)
            else:
                message += addRow(eventInfo, eventName, settings.UNKNOWN, '', '', '')
        number += 1
    message += "</table>"
    print ('Count event: ' + str(number))
    mail.sendMessageList(message)

def emptyToZero(str):
    if str == '':
        return '0'
    else:
        return str

def addRow(eventInfo, eventName, issue, paramName, paramValue, paramType):

    rowColor = ''

    if issue == settings.OFF:
        rowColor = 'lightpink'
    elif issue == settings.NOTONLIST:
        rowColor = 'lightcoral'
    elif issue == settings.DONTCOME:
        rowColor = 'lightyellow'
    elif issue == settings.EMPTY:
        rowColor = 'lightcyan'
    elif issue == settings.UNKNOWN:
        rowColor = 'lightgrey'
    else:
        rowColor = 'white'

    id = eventInfo.split(' => ')[0]
    date = eventInfo.split(' => ')[1]

    message = ''
    message += "<tr bgcolor = " + rowColor + ">"
    message += "<td>" + id + "</td><td>" + date + "</td><td>" + eventName + "</td><td>" + issue + "</td><td>" + paramName + "</td><td>" + paramValue + "</td><td>" + paramType + "</td>"
    message += "</tr>"
    return message



















