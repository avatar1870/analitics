import smtplib
import settings

def sendMessageList(message):
    listEmail = settings.TO.split(" ")
    for email in listEmail:
        sendMessage(email, message)

def sendMessage(to, message):
    to = to
    login = settings.LOGIN
    pwd = settings.PASSWORD
    smtpserver = smtplib.SMTP(settings.SMTPSERVER, settings.SMTPPORT)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo
    smtpserver.login(login, pwd)
    header = 'To:' + to + '\n' + 'From: ' + login + '\n' + 'Subject:' + settings.SUBJECT + '\n' + 'MIME-Version: 1.0' + '\n' + 'Content-type: text/html' + '\n'

    message = header + '\n' + message + '\n\n'
    smtpserver.sendmail(login, to, message)
    smtpserver.close()



